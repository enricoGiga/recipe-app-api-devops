#!/bin/sh
set -e  #to log any errors in the command line just to make easier to debug

#in Django is raccomanded to serve the static files to proxy, the reason of this is uWSGI is very good at
# serving Python applications, however it is not optimized for serving static files that's why I will
# use a proxy to serve them

# How we can get the static files from our Django project into the proxy?
#well, Django provide a handy management command call 'Collect static', It allows you to collect
# all of the static files required for your project and store them in a single directory
#so let's do it:

python manage.py collectstatic --noinput
python manage.py wait_for_db #we wait just a few seconds in order to be sure that our application wait for db
python manage.py migrate #when you make change to your Django database project you want that changes to be
# reflected into your database (That could be something like adding or removing a table, add a filed in a
# table , etc etc ) that's what this command do whenever Django start a new application.


# uwsgi is the command to run a new uwsgi application
# -socket :9000: this says run uWSGI as a TCP socket on port 9000, in this way we can map the requests form
# our proxy to this port using uWSGI port that we set up in our proxy application
# --enable-threads enable multi-threading in our web application

# this will run the uWSGI server
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi

