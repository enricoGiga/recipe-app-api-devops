# data is just for retriving information, it doesn't actually create or manage any resources
# doc https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami.html
data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["al2023-ami-2023.1.20230705.0-kernel-6.1-x86_64"] # the AMI name

  }
  owners = ["amazon"]
}


resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"
  tags = merge(
    local.common_tags,
  map("Name", "${local.prefix}-bastion"))
}