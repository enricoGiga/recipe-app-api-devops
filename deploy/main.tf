terraform {
  # store the terraform state on S3
  backend "s3" {
    bucket = "recipe-app-api-devops-en-tfstate"
    key    = "recipe-app.tfstate"
    #inside our bucket we save our key, which is going to contain our terraform state
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-en-state-lock"
  }
}
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}
# terraform syntax for interpolations that allows you to concatenate string from different resources and variables
# within terraform, terraform.workspace is provided by terraform to get your current workspace
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }

}